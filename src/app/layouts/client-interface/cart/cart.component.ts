/**
 * Created by andrew.yang on 7/31/2017.
 */
import { Component } from '@angular/core';
import {CartBaseComponent} from "./cart-base.component";
import { CartService } from 'src/app/cart.service';
import { UserService } from 'src/app/user.service';

@Component({
    selector: 'app-cart-page',
    styleUrls: ["cart.component.css"],
    templateUrl: 'cart.component.html'
})
export class CartComponent extends CartBaseComponent{
    error = false;
    constructor(protected cartService: CartService, private userService:UserService) {
        super(cartService);
    }

    ngOnInit() {

    }
    changeQuantity = (cart,quantity) => {
        cart.quantity = quantity;
        this.cartService.reloadCart(this.cartList);
    }

    checkout(price){
        this.error=false;
        console.log(price);
        // Appel backend pour le checkout
        /*
        this.userService.checkout(price, user).subscribe(data=>{
            
        }, error=>{
            this.error = true;
        })
        */
    }
}
