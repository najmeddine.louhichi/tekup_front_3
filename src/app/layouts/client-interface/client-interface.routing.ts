import { Routes } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';



export const ClientInterfaceRoutes: Routes = [
    {path: '', component: CategoryComponent},
    {path: 'cart', component: CartComponent},
    {path: 'category', component: CategoryComponent},
    {path: ':id', component: ProductComponent}

];
