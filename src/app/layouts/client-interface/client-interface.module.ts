import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ClipboardModule} from 'ngx-clipboard';
import {ClientInterfaceRoutes} from './client-interface.routing';
import {QuantityControlComponent} from './quantity-control/quantity-control.component';



@NgModule({
  declarations: [CategoryComponent, ProductComponent, CartComponent, QuantityControlComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(ClientInterfaceRoutes),
        FormsModule,
        HttpClientModule,
        NgbModule,
        ClipboardModule
    ],
})
export class ClientInterfaceModule { }
