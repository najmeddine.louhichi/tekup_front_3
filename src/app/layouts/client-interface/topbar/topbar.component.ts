/**
 * Created by andrew.yang on 7/28/2017.
 */
import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';

@Component({
    selector: 'top-bar',
    styleUrls: ['./topbar.component.css'],
    templateUrl: './topbar.component.html'

})
export class TopbarComponent implements OnInit {
    public collapse = false;
    public cart_num: number;
    constructor(
        private cartService: CartService
    ) { }

    ngOnInit() {
        this.cartService.cartListSubject
            .subscribe(res => {
                this.cart_num = res.length;
            });
    }
    toggleCartPopup = (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.cartService.toggleCart();
    }
}
