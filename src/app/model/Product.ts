import {Category, SubCategory} from './Category';

export class Product {
    id: any;
    designation: string;
    qte: number;
    dispo: boolean;
    prix: any;
    category: Category;
    subCategory: SubCategory;
}
