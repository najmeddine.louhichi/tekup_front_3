import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../product.service';
import {Product} from '../model/Product';
import {CategoryService} from '../category.service';
import {forkJoin} from 'rxjs';

@Component({
    selector: 'app-update-product',
    templateUrl: './update-product.component.html',
    styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

    id: any;
    product: Product = new Product();
    categoryList;

    constructor(private route: ActivatedRoute, private router: Router,
                private _productService: ProductService, private categoryService: CategoryService) {
    }

    ngOnInit(): void {
        this.id = this.route.snapshot.params['id'];
        forkJoin([this._productService.getProduct(this.id), this.categoryService.getCategorysList()])
            .subscribe(data => {
                console.log(data);
                this.categoryList = data[1];
                this.product = data[0];
            }, error => console.log(error));
    }

    updateProduct() {
        console.log(this.product);
        this._productService.updateProduct(this.id, this.product)
            .subscribe(data => {
                this.gotoList();
            }, error => console.log(error));
        this.product = new Product();
    }

    onSubmit() {
        this.updateProduct();
    }

    gotoList() {
        this.router.navigate(['/dashboard']);
    }
}
