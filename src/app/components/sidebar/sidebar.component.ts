import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/icons', title: 'Products',  icon:'ni ni-box-2 text-blue', class: '' },
    { path: '/addProduct', title: 'New Product',  icon:'ni ni-fat-add text-pink', class: '' },
    { path: '/tables', title: 'Providers',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/addFournisseur', title: 'New Provider',  icon:'ni ni text-orange', class: '' },
    { path: '/user-profile', title: 'Users',  icon:'ni ni-fat-add text-yellow', class: '' },
    { path: '/addUser', title: 'New User',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/categoryBrand', title: 'Categories',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/addCategory', title: 'New Category',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/login', title: 'Login',  icon:'ni-key-25 text-info', class: '' },
    { path: '/register', title: 'Register',  icon:'ni-circle-08 text-pink', class: '' }
  
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
