import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AddProductComponent } from './add-product/add-product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { AddFournisseurComponent } from './add-fournisseur/add-fournisseur.component';
import { UpdateFournisseurComponent } from './update-fournisseur/update-fournisseur.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { CategoryBrandComponent } from './category-brand/category-brand.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { CartService } from './cart.service';
import { ClientInterfaceComponent } from './layouts/client-interface/client-interface.component';
import { ClientInterfaceModule } from './layouts/client-interface/client-interface.module';
import { TopbarComponent } from './layouts/client-interface/topbar/topbar.component';
import { ProductService } from './product.service';
import { QuantityControlComponent } from './layouts/client-interface/quantity-control/quantity-control.component';
import { CartPopupComponent } from './layouts/client-interface/cart/cart-popup/cart-popup.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {UserProfileComponent} from './pages/user-profile/user-profile.component';
import {TablesComponent} from './pages/tables/tables.component';
import {ProductsComponent} from './pages/icons/products.component';
import {MapsComponent} from './pages/maps/maps.component';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ComponentsModule,
        NgbModule,
        RouterModule,
        AppRoutingModule
    ],
    declarations: [
        ClientInterfaceComponent,
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        AddProductComponent,
        UpdateProductComponent,
        AddFournisseurComponent,
        UpdateFournisseurComponent,
        AddUserComponent,
        UpdateUserComponent,
        CategoryBrandComponent,
        AddCategoryComponent,
        UpdateCategoryComponent,
        TopbarComponent,
        CartPopupComponent,
        DashboardComponent,
        UserProfileComponent,
        TablesComponent,
        ProductsComponent,
        MapsComponent
    ],
    providers: [CartService, ProductService],
    exports: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
