import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../model/user';
@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
   
  idUser: any;
  user: User;
  constructor(private route: ActivatedRoute,private router: Router,
    private _userService: UserService) { }

  ngOnInit(): void {
    

    this.user = new User();
    this.idUser = this.route.snapshot.params['idUser'];
    this._userService.getUser(this.idUser)
    .subscribe(data => {
      console.log(data)
      this.user = data;
    }, error => console.log(error));

  }
  updateUser() {
    this._userService.updateUser(this.idUser, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    this.user = new User();
    this.gotoList();
  }

  onSubmit() {
    this.updateUser();    
  }

  gotoList() {
    this.router.navigate(['/user-profile']);
  }

}
